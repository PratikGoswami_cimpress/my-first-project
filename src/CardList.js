import Card from "./Card";
function CardList(props){
    const profiles=props.profile;
    return (
        <>
        <Card profile={profiles[0]}/>
        <Card profile={profiles[1]}/>
        <Card profile={profiles[2]}/>
        </>
    )
}
export default CardList;