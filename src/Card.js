import "bootstrap/dist/css/bootstrap.min.css";
import "./Card.css";
import { Link } from "react-router-dom";
import  Button  from  './Button';
function Card(props){
  
  const profile_url = '/ProfileDetails/'+props.profile.id;
    return(
<div>
      <div className="profile-card shadow-sm p-3 mb-5 rounded">
        <div>
         <img src={props.profile.image_url}></img>
         </div>
        <div className="contents">
          <ul>
            <li>
              <b>{props.profile.username}</b>
            </li>
            <li>{props.profile.age} Years</li>
            <li>{props.profile.dateOfBirth} </li>
           
          </ul>
          <p>{props.profile.gender}</p>
          <p>
            {props.profile.description}
          </p>
          <Link to={profile_url}>
            <Button/>
          </Link>
        </div>
      </div>
    </div>
    );
}
export default Card;