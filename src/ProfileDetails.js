import "./ProfileDetails.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {Link, useParams} from 'react-router-dom'

function ProfileDetails(props){


const { slug } = useParams(); 
const profile = props.profile[slug-1];



    return (
        <div className="container shadow-sm p-3 mb-5 bg-body rounded">
        <div>
          <div className="basic-card">
          <p> <img src={profile.image_url}></img></p>
            <div className="profile-contents">
              <ul>
                <li>
                  <b>{profile.username}</b>
                </li>
                <li>{profile.dateOfBirth}</li>
                <li> {profile.age} Years</li>
              </ul>
              <p>{profile.gender}</p>
            </div>
          </div>
          <br />
          <div>
            <p>
              {profile.description}
            </p>
            <Link to='/card-List'><button className="btn back-btn btn-outline-primary">Back</button></Link>
          </div>
        </div>
      </div>
    );
}
export default ProfileDetails;