import Card from "./Card";
import "./App.css";
import ProfileDetails from "./ProfileDetails";
import "bootstrap/dist/css/bootstrap.min.css";
import {Routes,Route} from 'react-router-dom'
import Login  from "./Login";
import CardList from "./CardList";

function App() {
  const profiles = [
    {
      id: "1",
      image_url:
        "https://media.istockphoto.com/id/1249420269/photo/young-smiling-handsome-man-in-casual-clothes-posing-isolated-on-blue-wall-background-studio.webp?s=612x612&w=is&k=20&c=S-_kQ15FuNovZS-OOzPtWp81o0AditonAefVpVv2dA0=",
      username: "Pratik Goswami",
      age: "22",
      dateOfBirth: "27-10-1999",
      gender: "Male",
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum
      et euismod nisi, sit amet auctor velit. Duis quis auctor dui, et
      scelerisque risus. Nunc ultrices interdum neque, vel tristique erat.`,
    },
    {
      id: "2",
      image_url:
        "https://media.istockphoto.com/photos/handsome-latin-man-against-turquoise-background-picture-id1182055260?k=20&m=1182055260&s=612x612&w=0&h=xEbI-6S6X55CdJKtlQbhiZjsCuVj2rJ1q7k87V8KJOs=",
      username: "Amit Pandey",
      age: "23",
      dateOfBirth: "01-01-1999",
      gender: "Male",
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum
      et euismod nisi, sit amet auctor velit. Duis quis auctor dui, et
      scelerisque risus. Nunc ultrices interdum neque, vel tristique erat.`,
    },
    {
      id: "3",
      image_url:
        "https://images.unsplash.com/photo-1509839862600-309617c3201e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fHByb2ZpbGUlMjBwaG90b3xlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60",
      username: "Riya Nath",
      age: "23",
      dateOfBirth: "01-01-1999",
      gender: "Female",
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum
      et euismod nisi, sit amet auctor velit. Duis quis auctor dui, et
      scelerisque risus. Nunc ultrices interdum neque, vel tristique erat.`,
    },
  ];

  console.log(profiles, "printing profiles");

  return (
    <div>
      <h2>Hello there!</h2>
      <Routes>

      
      <Route path='/' element={<Login/>}/>
     
<Route path="/card-List" element= {<CardList  profile={profiles}/>}/>

      
      
      <Route path="/ProfileDetails/:slug" element=  {<ProfileDetails profile={profiles} />}>
          
        </Route>
      </Routes>
    </div>
  );
}

export default App;
